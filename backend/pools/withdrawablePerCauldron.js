import fetch from "node-fetch";
import Web3 from "web3";
import { createRequire } from "module";
import config from "../config.js";
import { getRPCForChain, getValueForDecimals } from "../utils.js";

const require = createRequire(import.meta.url);
const CauldronABI = require("../datas/abi/CauldronABI.json");
const DegenBoxABI = require("../datas/abi/DegenBoxABI.json");
const ERC20ABI = require("../datas/abi/ERC20ABI.json");

export const withdrawablePerCauldrons = async () => {
  let pools = [];
  const response = await fetch(
    `https://analytics.abracadabra.money/api/pools`,
    {
      method: "GET",
    }
  );
  const jsonResponse = await response.json();
  pools = jsonResponse.pools
    .sort((a, b) => a.name.localeCompare(b.name))
    .sort((a, b) => a.network - b.network);
  pools = pools.filter((pool) => !pool.name.includes("dep-"));
  let withdrawablePools = [];
  for (let i = 0; i < pools.length; i++) {
    const { name, address, network } = pools[i];
    const web3 = new Web3(getRPCForChain(network));
    const CauldronContract = new web3.eth.Contract(CauldronABI, address);
    const collateral = await CauldronContract.methods.collateral().call();
    const degenBox = await CauldronContract.methods.bentoBox().call();
    const DegenBoxContract = new web3.eth.Contract(DegenBoxABI, degenBox);
    const strategy = await DegenBoxContract.methods.strategy(collateral).call();
    if (strategy !== "0x0000000000000000000000000000000000000000") {
      console.log("name : ", name);
      console.log("collateral : ", collateral);
      console.log("degenBox : ", degenBox);
      console.log("strategy : ", strategy);
      const CollateralContract = new web3.eth.Contract(ERC20ABI, collateral);
      const decimals = await CollateralContract.methods.decimals().call();
      console.log("decimals : ", decimals);
      const balance = await CollateralContract.methods
        .balanceOf(degenBox)
        .call();
      const withdrawableAmount = getValueForDecimals(balance, decimals);
      console.log("withdrawableAmount : ", withdrawableAmount);
      withdrawablePools.push({
        ...pools[i],
        collateral,
        degenBox,
        withdrawableAmount,
      });
    }
  }
  console.log("pools : ", withdrawablePools);
  return withdrawablePools;
};
