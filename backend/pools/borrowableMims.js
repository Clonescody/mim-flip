import Web3 from "web3";
import { createRequire } from "module";
import { createClient } from "redis";
import { getChainNameFromId, getRPCForChain } from "../utils.js";

let redis = null;

(async () => {
  redis = createClient();

  redis.on("error", (err) => console.log("Redis Client Error", err));

  await redis.connect();
})();

const require = createRequire(import.meta.url);
const BentoBoxABI = require("../datas/abi/BentoBoxABI.json");
const CauldronABI = require("../datas/abi/CauldronABI.json");

export const getBorrowableAndBorrowedMims = async () => {
  const cauldronsRaw = await redis.get("byebyedai.poolsList");
  const cachedBorrowableAndBorrowedRaw = await redis.get(
    "byebyedai.borrowedAndBorrowableMims"
  );
  const cachedBorrowableAndBorrowed = JSON.parse(
    cachedBorrowableAndBorrowedRaw
  );
  const cauldrons = JSON.parse(cauldronsRaw);
  let borrowedAndBorrowables = [];
  for (let i = 0; i < cauldrons.length; i++) {
    const { name, network, address, deprecated } = cauldrons[i];
    const web3 = new Web3(getRPCForChain(network));
    try {
      const block = await web3.eth.getBlockNumber();
      const CauldronContract = new web3.eth.Contract(CauldronABI, address);

      const BentoBoxAddress = await CauldronContract.methods
        .bentoBox()
        .call(null, block);
      const MIMAddress = await CauldronContract.methods
        .magicInternetMoney()
        .call(null, block);
      const BentoBoxContract = new web3.eth.Contract(
        BentoBoxABI,
        BentoBoxAddress
      );

      const borrowedMIMs = await CauldronContract.methods
        .totalBorrow()
        .call(null, block);
      const borrowableMIMs = await BentoBoxContract.methods
        .balanceOf(MIMAddress, address)
        .call(null, block);

      borrowedAndBorrowables.push({
        name,
        chain: getChainNameFromId(network),
        borrowableMIMs: parseInt(Web3.utils.fromWei(borrowableMIMs)),
        borrowedMIMs: parseInt(Web3.utils.fromWei(borrowedMIMs["1"])),
        deprecated,
      });
    } catch (error) {
      console.log(
        `Error on ${getChainNameFromId(network)} for ${name} : `,
        error
      );
      borrowedAndBorrowables.push(
        cachedBorrowableAndBorrowed.find((item) => item.name === name)
      );
    }
  }
  await redis.set(
    "byebyedai.borrowedAndBorrowableMims",
    JSON.stringify(borrowedAndBorrowables)
  );
  console.log("Cauldrons ", borrowedAndBorrowables);
  return borrowedAndBorrowables;
};
