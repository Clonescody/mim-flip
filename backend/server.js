import express from "express";
import cors from "cors";
import CacheService from "express-api-cache";
import timeout from "connect-timeout";
import { createClient } from "redis";
import config from "./config.js";
import { getBorrowableAndBorrowedMims } from "./pools/borrowableMims.js";
import moment from "moment";
import { TREASURY_ADDRESS } from "./datas/constants/Addresses.js";
import { getTreasury } from "./feesCalculator/treasury.js";
import { getHistoricalStakingRatio } from "./staking/sSpellRatio.js";
import { getUserLiquidations } from "./helpers/getUserLiquidations.js";
import { withdrawablePerCauldrons } from "./pools/withdrawablePerCauldron.js";

const PORT = 5001;
const app = express();
const cache = CacheService.cache;

app.use(cors());
app.use(timeout(600000));
app.use(express.json());

let redis = null;

(async () => {
  redis = createClient();

  redis.on("error", (err) => console.log("Redis Client Error", err));

  await redis.connect();
})();

const corsOptions = {
  origin:
    config.env === "dev"
      ? "http://localhost:3001"
      : "https://www.abrastats.money",
};

app.get("/getMkrFees", cache("1 hour"), cors(corsOptions), async (req, res) => {
  const makerFees = await redis.get("byebyedai.makerFees");
  res.json(JSON.parse(makerFees));
});

app.get(
  "/getSpellFees",
  cache("1 hour"),
  cors(corsOptions),
  async (req, res) => {
    const total = await redis.get("byebyedai.totalSpellFees");
    res.json(JSON.parse(total));
  }
);

app.get(
  "/getSpellCauldrons",
  cache("1 hour"),
  cors(corsOptions),
  async (req, res) => {
    const cauldronsArray = await redis.get("byebyedai.cauldronsArray");
    res.send({
      cauldrons: JSON.parse(cauldronsArray),
    });
  }
);

app.get(
  "/getHistoricalStakedSpellRatio",
  cache("1 hour"),
  cors(corsOptions),
  async (req, res) => {
    const stakedRatioHistory = await getHistoricalStakingRatio();
    res.send({
      stakedRatioHistory: stakedRatioHistory,
    });
  }
);

app.get(
  "/getEmissions",
  cache("1 hour"),
  cors(corsOptions),
  async (req, res) => {
    const emissions = await redis.get(
      "byebyedai.totalEmissionsLastSevenDaysUSD"
    );
    res.json(JSON.parse(emissions));
  }
);

app.get(
  "/getBorrowableMims",
  cors(corsOptions),
  cache("1 hour"),
  async (req, res) => {
    console.log("Update borrowable MIMs...", moment().format("HH:mm:ss"));
    const borrowableAndBorrowedMim = await getBorrowableAndBorrowedMims();
    console.log("Update done.", moment().format("HH:mm:ss"));
    res.json({
      cauldrons: borrowableAndBorrowedMim,
    });
  }
);

app.get(
  "/getTreasury",
  cors(corsOptions),
  cache("1 hour"),
  async (req, res) => {
    console.log("Get treasury ...");
    const { treasury, totalValueUsd, totalValueEth, totalValueBtc } =
      await getTreasury();
    const treasuryAddress = TREASURY_ADDRESS;
    console.log("Treasury fetched !");
    res.json({
      treasuryAssets: treasury,
      totalValueUsd,
      totalValueEth,
      totalValueBtc,
      treasuryAddress,
    });
  }
);

app.get(
  "/getDailyFees",
  cors(corsOptions),
  cache("1 hour"),
  async (req, res) => {
    console.log("Get daily fees ...");
    const dailyFees = await redis.get("byebyedai.dailyFees");
    console.log("Daily fees fetched !");
    res.json({
      dailyFees: JSON.parse(dailyFees),
    });
  }
);

app.post(
  "/getTotalUserLiquidations",
  cors(corsOptions),
  cache("1 hour"),
  async (req, res) => {
    console.log(`Get ${req.body.address.toLowerCase()} liquidations ...`);
    const totals = await getUserLiquidations(req.body.address.toLowerCase());
    console.log("Liquidations fetched !");
    res.json(totals);
  }
);

app.get(
  "/getWithdrawableAmounts",
  cors(corsOptions),
  cache("15 seconds"),
  async (req, res) => {
    console.log(`Get withdrawable amounts for cauldrons ...`);
    const cauldrons = await withdrawablePerCauldrons();
    console.log("Withdrawable amounts fetched !");
    res.json(cauldrons);
  }
);

app.listen(PORT, "127.0.0.1", () => {
  console.log(`Server listening for ${corsOptions.origin} requests ...`);
});
