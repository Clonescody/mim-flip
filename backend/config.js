const config = {
  env: process.env.NODE_ENV,
  covalentApiUrl: "https://api.covalenthq.com/v1",
  covalentApiKey: process.env.COVALENT_API_KEY,
  cryptoFeesApiUrl: "https://cryptofees.info/api/v1",
  abracadabraApiUrl: process.env.ABRACADABRA_API_URL,
  coingeckoApiUrl: "https://api.coingecko.com/api/v3",
};

export default config;
