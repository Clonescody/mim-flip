export const StakedSpellRatioQuery = `
query {
  ratioUpdates(subgraphError: allow) {
    timestamp
    ratio
  }
}
`;
