export const ETH_RPC_URL = "https://rpc.flashbots.net";
export const AVALANCHE_RPC_URL = "https://api.avax.network/ext/bc/C/rpc";
export const FANTOM_RPC_URL = "https://rpc.ftm.tools";
export const ARBITRUM_RPC_URL = "https://arb1.arbitrum.io/rpc";
export const BINANCE_RPC_URL = "https://bsc-dataseed1.binance.org:443";
