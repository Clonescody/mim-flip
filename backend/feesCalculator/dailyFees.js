import moment from "moment";
import { createClient } from "redis";

import fetch from "node-fetch";
import Web3 from "web3";
import config from "../config.js";

let redis = null;

(async () => {
  redis = createClient();

  redis.on("error", (err) => console.log("Redis Client Error", err));

  await redis.connect();
})();

const getDailyFees = async () => {
  let dailyFees = [];
  let cumulativeFees = 0.0;
  let from = moment("2021-06-01").toISOString();
  const storedDailyFeesRaw = await redis.get("byebyedai.dailyFees");
  const storedDailyFees = JSON.parse(storedDailyFeesRaw);

  if (storedDailyFees !== null) {
    from = moment(
      storedDailyFees[storedDailyFees.length - 1].timestamp
    ).toISOString();
  }
  const days = moment().diff(moment(from), "d");
  for (let i = 0; i < days; i++) {
    const responseFees = await fetch(
      `${config.abracadabraApiUrl}/statistic/fees-earned`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          date: {
            from: moment(from).add(i, "d").toISOString(),
            to: moment(from)
              .add(i + 1, "d")
              .toISOString(),
          },
        }),
      }
    );

    const jsonResponseFees = await responseFees.json();

    if (storedDailyFees !== null) {
      cumulativeFees =
        parseFloat(storedDailyFees[storedDailyFees.length - 1].cumulativeFees) +
        parseFloat(Web3.utils.fromWei(jsonResponseFees.totalFeesEarned));
    } else {
      cumulativeFees += parseFloat(
        Web3.utils.fromWei(jsonResponseFees.totalFeesEarned)
      );
    }
    if (storedDailyFees !== null) {
      storedDailyFees.push({
        timestamp: moment(from).add(1, "d").valueOf(),
        fees: parseFloat(Web3.utils.fromWei(jsonResponseFees.totalFeesEarned)),
        cumulativeFees: parseFloat(cumulativeFees).toFixed(0),
      });
    } else {
      dailyFees.push({
        timestamp: moment(from).add(i, "d").valueOf(),
        fees: parseFloat(Web3.utils.fromWei(jsonResponseFees.totalFeesEarned)),
        cumulativeFees: parseFloat(cumulativeFees).toFixed(0),
      });
    }
  }
  if (storedDailyFees !== null) {
    await redis.set("byebyedai.dailyFees", JSON.stringify(storedDailyFees));
  } else {
    await redis.set("byebyedai.dailyFees", JSON.stringify(dailyFees));
  }
};

await getDailyFees();
process.exit(0);
