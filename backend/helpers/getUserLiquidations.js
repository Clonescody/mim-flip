import pkg from "@apollo/client/core/core.cjs";
import { createHttpLink } from "apollo-link-http";
import fetch from "node-fetch";
import { UserLiquidationsQuery } from "../datas/queries/UserLiquidationsQuery.js";
import { getChainNameFromId } from "../utils.js";

const chainResources = [
  {
    id: 1,
    name: "Ethereum",
    explorer: "https://etherscan.io/tx/",
    subgraph:
      "https://api.thegraph.com/subgraphs/name/ap0calyp/abracadabra-mainnet-fees",
  },
  {
    id: 42161,
    name: "Arbitrum",
    explorer: "https://arbiscan.io/tx/",
    subgraph:
      "https://api.thegraph.com/subgraphs/name/ap0calyp/abracadabra-arbitrum-fees",
  },
  {
    id: 250,
    name: "Fantom",
    explorer: "https://ftmscan.com/tx/",
    subgraph:
      "https://api.thegraph.com/subgraphs/name/ap0calyp/abracadabra-fantom-fees",
  },
  {
    id: 43114,
    name: "Avalanche",
    explorer: "https://snowtrace.io/tx/",
    subgraph:
      "https://api.thegraph.com/subgraphs/name/ap0calyp/abracadabra-avalanche-fees",
  },
  {
    id: 56,
    name: "Binance Smart Chain",
    explorer: "https://bscscan.com/tx/",
    subgraph:
      "https://api.thegraph.com/subgraphs/name/ap0calyp/abracadabra-binancesmartchain-fees",
  },
];
const { ApolloClient, InMemoryCache, gql } = pkg;

const getLiquidationsFromGraph = async (address, chainId, subgraph) => {
  const apolloClient = new ApolloClient({
    link: createHttpLink({
      uri: subgraph,
      fetch: fetch,
    }),
    cache: new InMemoryCache(),
  });

  const infos = await apolloClient.query({
    query: gql(UserLiquidationsQuery),
    variables: {
      address,
    },
  });
  return infos.data.userLiquidations.map((liq) => {
    const {
      transaction,
      exchangeRate,
      timestamp,
      loanRepaid,
      collateralRemoved,
      cauldron,
    } = liq;
    const { collateralSymbol } = cauldron;
    return {
      transaction,
      exchangeRate,
      timestamp,
      loanRepaid,
      collateralRemoved,
      collateralSymbol,
      chainId,
    };
  });
};

const getLiquidations = async (address) => {
  let liquidations = [];
  for (let i = 0; i < chainResources.length; i++) {
    const { id, subgraph } = chainResources[i];
    const liqu = await getLiquidationsFromGraph(address, id, subgraph);

    if (liqu.length > 0) {
      liquidations.push({ id, liquidations: liqu });
    }
  }
  return liquidations;
};

const calculateTotalLiquidated = (allLiquidations, address) => {
  let totalByChain = [];
  for (let i = 0; i < allLiquidations.length; i++) {
    const { id, liquidations } = allLiquidations[i];
    let assetsList = [];
    liquidations.map((item) => {
      if (!assetsList.includes(item.collateralSymbol)) {
        assetsList.push(item.collateralSymbol);
      }
    });
    let totalByAsset = [];
    for (let j = 0; j < assetsList.length; j++) {
      let totalForAsset = 0.0;
      let totalCollateral = 0.0;
      liquidations
        .filter((item) => item.collateralSymbol === assetsList[j])
        .map((item) => {
          totalForAsset +=
            parseFloat(item.collateralRemoved) / parseFloat(item.exchangeRate) -
            parseFloat(item.loanRepaid);
          totalCollateral += parseFloat(item.collateralRemoved);
        });
      totalByAsset.push({
        asset: assetsList[j],
        total: `${totalForAsset}`,
        totalCollateral: `${totalCollateral}`,
      });
    }
    let totalForChain = 0.0;
    totalByAsset.map((item) => {
      totalForChain += parseFloat(item.total);
    });
    totalByChain.push({
      id,
      name: getChainNameFromId(id),
      total: `${totalForChain}`,
      totalByAsset,
    });
  }
  let totalLiquidated = 0.0;
  totalByChain.map((item) => {
    totalLiquidated += parseFloat(item.total);
  });
  console.log(`Total liquidated on account ${address} : ${totalLiquidated}`);
  return {
    totalByChain,
    totalLiquidated: `${totalLiquidated}`,
  };
};

export const getUserLiquidations = async (address) => {
  const liquidations = await getLiquidations(address);
  const totals = calculateTotalLiquidated(liquidations, address);
  return totals;
};
