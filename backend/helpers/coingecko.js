import fetch from "node-fetch";
import config from "../config.js";

export const getPrice = async (tokenId) => {
  const responseToken = await fetch(
    `${config.coingeckoApiUrl}/coins/${tokenId}`
  );
  const datasToken = await responseToken.json();
  const tokenPrice = datasToken.market_data.current_price.usd;
  return tokenPrice;
};
