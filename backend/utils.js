import Web3 from "web3";
import { createRequire } from "module";

import {
  ARBITRUM_RPC_URL,
  AVALANCHE_RPC_URL,
  BINANCE_RPC_URL,
  ETH_RPC_URL,
  FANTOM_RPC_URL,
} from "./datas/constants/RPCs.js";

const require = createRequire(import.meta.url);
const CauldronABI = require("./datas/abi/CauldronABI.json");
const OracleABI = require("./datas/abi/OracleABI.json");
const ERC20ABI = require("./datas/abi/ERC20ABI.json");

export const getFeesForProtocol = (protocolsArray, protocolId) => {
  let fees = 0.0;
  protocolsArray
    .filter((item) => item.id === protocolId)[0]
    .fees.forEach((dailyFee) => {
      fees += dailyFee.fee;
    });
  return fees;
};

export const getRPCForChain = (id) => {
  switch (id) {
    default:
    case 1:
      return ETH_RPC_URL;
    case 56:
      return BINANCE_RPC_URL;
    case 250:
      return FANTOM_RPC_URL;
    case 42161:
      return ARBITRUM_RPC_URL;
    case 43114:
      return AVALANCHE_RPC_URL;
  }
};

export const getChainNameFromId = (id) => {
  switch (id) {
    default:
    case 1:
      return "Ethereum";
    case 56:
      return "Binance";
    case 250:
      return "Fantom";
    case 42161:
      return "Arbitrum";
    case 43114:
      return "Avalanche";
  }
};

export const getValueForDecimals = (value, decimals = 18) => {
  const unit = Object.keys(Web3.utils.unitMap).find(
    (key) =>
      Web3.utils.unitMap[key] ===
      Web3.utils.toBN(10).pow(Web3.utils.toBN(decimals)).toString()
  );
  return Web3.utils.fromWei(`${value}`, unit);
};

export const getDecimalsOfToken = async (token, chain = 1) => {
  try {
    const web3 = new Web3(getRPCForChain(chain));
    const TokenContract = new web3.eth.Contract(ERC20ABI, token);

    const decimals = await TokenContract.methods.decimals().call();
    return decimals;
  } catch (error) {
    console.log("Error decimals ", error);
    return 18;
  }
};

export const getCollateralAddressOfCauldron = async (cauldron, chain = 1) => {
  const web3 = new Web3(getRPCForChain(chain));
  const CauldronContract = new web3.eth.Contract(CauldronABI, cauldron);

  const collateral = await CauldronContract.methods.collateral().call();
  return collateral;
};

export const getOraclePriceOfCauldron = async (cauldron) => {
  const web3 = new Web3(getRPCForChain(1));
  const CauldronContract = new web3.eth.Contract(CauldronABI, cauldron);

  const oracle = await CauldronContract.methods.oracle().call();
  const oracleData = await CauldronContract.methods.oracleData().call();
  const OracleContract = new web3.eth.Contract(OracleABI, oracle);
  const oraclePrice = await OracleContract.methods.get(oracleData).call();

  return oraclePrice["1"];
};
