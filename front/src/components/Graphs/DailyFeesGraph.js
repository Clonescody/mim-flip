import "../../styles/graphs/DailyFeesGraph.css";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import colors from "../../styles/colors";
import NumberFormat from "react-number-format";

const DailyFeesGraph = ({ dailyFees }) => {
  let resizeCallToClear = null;
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(
        setDimensions({
          height: window.innerHeight,
          width: window.innerWidth,
        }),
        100
      );
    }
    window.addEventListener("resize", handleResize);
  }, []);

  const options = {
    stroke: {
      show: true,
      width: [1, 2],
    },
    colors: [colors.secondary, "#ffffff", "#00ffb4"],
    xaxis: {
      type: "datetime",
      labels: {
        style: {
          colors: "white",
        },
      },
    },
    yaxis: [
      {
        title: {
          text: "Daily fees",
          style: {
            color: "white",
            fontSize: "18px",
          },
        },
        type: "numeric",
        labels: {
          style: {
            colors: ["white"],
            fontSize: "15px",
          },
          formatter: (value) => {
            return value.toLocaleString();
          },
        },
      },
      {
        opposite: true,
        title: {
          text: "Cumulative fees",
          style: {
            color: "white",
            fontSize: "18px",
          },
        },
        type: "numeric",
        labels: {
          style: {
            colors: ["white"],
            fontSize: "15px",
          },
          formatter: (value) => {
            return value.toLocaleString();
          },
        },
      },
    ],
    dataLabels: {
      enabled: false,
    },
    legend: {
      show: true,
      labels: {
        colors: ["white"],
      },
    },
    tooltip: {
      enabled: true,
      theme: "dark",
    },
  };

  const feesSerie = {
    name: "Daily fees in MIMs",
    type: "bar",
    data: dailyFees.map((dailyFee) => [
      `${dailyFee.timestamp}`.length === 16
        ? dailyFee.timestamp / 1000
        : dailyFee.timestamp,
      dailyFee.fees.toFixed(0),
    ]),
  };

  const cumulativeFeesSerie = {
    name: "Cumulative fees in MIMs",
    type: "line",
    data: dailyFees.map((dailyFee) => [
      `${dailyFee.timestamp}`.length === 16
        ? dailyFee.timestamp / 1000
        : dailyFee.timestamp,
      parseFloat(dailyFee.cumulativeFees).toFixed(0),
    ]),
  };

  // const twoWeeksAverageSerie = {
  //   name: "14 days average",
  //   type: "line",
  //   data: dailyFees.map((dailyFee) => [
  //     `${dailyFee.timestamp}`.length === 16
  //       ? dailyFee.timestamp / 1000
  //       : dailyFee.timestamp,
  //     isNaN(parseFloat(dailyFee.twoWeeksAverage).toFixed(0))
  //       ? 0.0
  //       : parseFloat(dailyFee.twoWeeksAverage).toFixed(0),
  //   ]),
  // };

  const { width } = dimensions;

  return (
    <div className="DailyFees-Graph-container">
      <p className="DailyFees-Graph-title">
        {"Daily fees generated since inception"}
      </p>
      <NumberFormat
        suffix=" MIMs total"
        value={dailyFees[dailyFees.length - 1].cumulativeFees}
        displayType="text"
        thousandSeparator
        renderText={(value) => (
          <p className="DailyFees-Graph-amount">{value}</p>
        )}
      />
      <Chart
        options={options}
        series={[feesSerie, cumulativeFeesSerie]}
        width={width - (width / 100) * 3}
        height={"450px"}
      />
    </div>
  );
};

DailyFeesGraph.propTypes = {
  dailyFees: PropTypes.array,
};

export default DailyFeesGraph;
