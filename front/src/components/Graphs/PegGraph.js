import "../../styles/graphs/PegGraph.css";
import moment from "moment-timezone";
import { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import colors from "../../styles/colors";
import { getChartWidthForSize } from "../../utils/helpers";
import MyLoader from "../Loader";
import { getChainLinkPriceFeed } from "../../utils/getChainLinkPriceFeed";

const PEG_FEEDS = [
  {
    asset: "MIM",
    feedAddress: "0x1a6E198c667223a4e1ecee7F5727E2A384210025",
  },
  {
    asset: "USDC",
    feedAddress: "0x789190466E21a8b78b8027866CBBDc151542A26C",
  },
  {
    asset: "USDT",
    feedAddress: "0xa964273552C1dBa201f5f000215F5BD5576e8f93",
  },
];

const PegGraph = () => {
  const [priceFeeds, setPriceFeeds] = useState([]);
  const [dayInterval, setDayInterval] = useState(30);
  const [isLoadingFeeds, setIsLoadingFeeds] = useState(false);
  const [feedsLoaded, setFeedsLoaded] = useState(false);
  const [width, setWidth] = useState(window.innerWidth);
  const [error, setError] = useState("");

  const timeInterval =
    (moment.tz("America/Costa_Rica").unix() -
      moment.tz("America/Costa_Rica").subtract(dayInterval, "d").unix()) /
    60;

  let resizeCallToClear = null;

  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(setWidth(window.innerWidth), 100);
    }
    window.addEventListener("resize", handleResize);
  }, []);

  const fetchFeeds = async () => {
    setIsLoadingFeeds(true);
    let tmpPriceFeeds = [];
    try {
      for (let i = 0; i < PEG_FEEDS.length; i++) {
        const { asset } = PEG_FEEDS[i];
        const priceFeed = await getChainLinkPriceFeed(
          PEG_FEEDS[i],
          dayInterval,
          timeInterval
        );
        tmpPriceFeeds.push({
          asset,
          serie: {
            name: `${asset} price feed`,
            data: priceFeed.map((price) => [price[0] * 1000, price[1]]),
          },
        });
      }
    } catch (error) {
      setError(`Failed pegs fetch : ${error.toString()}`);
      setIsLoadingFeeds(false);
      setFeedsLoaded(true);
    }
    setPriceFeeds(tmpPriceFeeds);
    setIsLoadingFeeds(false);
    setFeedsLoaded(true);
  };

  useEffect(() => {
    if (!isLoadingFeeds && priceFeeds.length === 0) {
      fetchFeeds();
    }
  }, []);

  useEffect(() => {
    if (priceFeeds.length > 0) {
      fetchFeeds();
    }
  }, [dayInterval]);

  if (isLoadingFeeds || !feedsLoaded) {
    return (
      <div className="PegGraph-container">
        <MyLoader />
      </div>
    );
  }

  if (error !== "") {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }
  const MIMPriceFeed = priceFeeds.find((feed) => feed.asset === "MIM");

  const PegGraphOptions = {
    stroke: {
      show: true,
      curve: "smooth",
    },
    colors: [colors.secondary, "#ffffff", "#00ffb4"],
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      categories: MIMPriceFeed.serie.data.map((item) => item[0] * 1000),
      labels: {
        style: {
          colors: "white",
        },
      },
    },
    yaxis: {
      labels: {
        style: {
          colors: "white",
        },
      },
      min: 0.99,
      max: 1.01,
      horizontalAlign: "center",
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm",
      },
      enabled: true,
      theme: "dark",
    },
    legend: {
      show: true,
      labels: {
        colors: ["white"],
      },
    },
  };

  return (
    <div className="PegGraph-container">
      <p className="PegGraph-container-title">{`Underlying stablecoin oracles over last ${
        dayInterval > 1 ? `${dayInterval} days` : "24hrs"
      }`}</p>
      {feedsLoaded &&
        priceFeeds.map((feed) => (
          <p key={feed.asset} className="PegGraph-subtitle">{`Current ${
            feed.asset
          } price : ${feed.serie.data[feed.serie.data.length - 1][1]}`}</p>
        ))}
      <div className="PegGraph-buttons-container">
        <button
          style={dayInterval === 30 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(30)}
        >
          30 days
        </button>
        <button
          style={dayInterval === 7 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(7)}
        >
          7 days
        </button>
        <button
          style={dayInterval === 2 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(2)}
        >
          2 days
        </button>
        <button
          style={dayInterval === 1 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(1)}
        >
          24 hrs
        </button>
      </div>
      <Chart
        options={PegGraphOptions}
        series={priceFeeds.map((feed) => feed.serie)}
        type="line"
        width={getChartWidthForSize(width)}
      />
      <p className="PegGraph-subtitle">{`Prices provided by Chainlink`}</p>
    </div>
  );
};

export default PegGraph;
