import "../../styles/graphs/HoldersGraph.css";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Chart from "react-apexcharts";
import NumberFormat from "react-number-format";
import { getGraphSizeForResponsive } from "../../utils/helpers";
import config from "../../config/config";

let responseSpellHoldersJson = [
  {
    chainId: "1",
    chainName: "Ethereum",
    holders: 8817,
  },
  {
    chainId: "250",
    chainName: "Fantom",
    holders: 7793,
  },
  {
    chainId: "42161",
    chainName: "Arbitrum",
    holders: 2957,
  },
  {
    chainId: "43114",
    chainName: "Avalanche",
    holders: 2595,
  },
];
let responsesSpellHoldersJson = [
  {
    chainId: "1",
    chainName: "Ethereum",
    holders: 4876,
  },
];
let responseMkrHoldersJson = [
  {
    chainId: "1",
    chainName: "Ethereum",
    holders: 81441,
  },
];

const HoldersGraph = () => {
  const [sSPELLHolders, setsSPELLHolders] = useState(responsesSpellHoldersJson);
  const [SPELLHolders, setSPELLHolders] = useState(responseSpellHoldersJson);
  const [MKRHolders, setMKRHolders] = useState(responseMkrHoldersJson);
  const [error, setError] = useState("");
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });
  let resizeCallToClear = null;

  const fetchHolders = async () => {
    try {
      const responseSpellHolders = await fetch(
        `${config.backend}/getSpellHolders`
      );
      responseSpellHoldersJson = await responseSpellHolders.json();

      const responsesSpellHolders = await fetch(
        `${config.backend}/getsSpellHolders`
      );
      responsesSpellHoldersJson = await responsesSpellHolders.json();

      const responseMkrHolders = await fetch(`${config.backend}/getMkrHolders`);
      responseMkrHoldersJson = await responseMkrHolders.json();
      setSPELLHolders(responseSpellHoldersJson);
      setsSPELLHolders(responsesSpellHoldersJson);
      setMKRHolders(responseMkrHoldersJson);
    } catch (error) {
      setError(error.toString());
      console.log("Error holders : ", error);
    }
  };

  useEffect(() => {
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV === "production") {
      fetchHolders();
    }
  }, []);

  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(
        setDimensions({
          height: window.innerHeight,
          width: window.innerWidth,
        }),
        100
      );
    }
    window.addEventListener("resize", handleResize);
  }, []);

  if (error !== "") {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  SPELLHolders.sort(
    (networkA, networkB) => networkA.holders - networkB.holders
  );

  const { width } = dimensions;

  const options = {
    legend: {
      show: true,
      position: "bottom",
      labels: {
        colors: ["white"],
      },
      fontSize: width < 650 ? "12px" : "17px",
      horizontalAlign: "center",
      width: getGraphSizeForResponsive(width),
      inverseOrder: true,
    },
    tooltip: {
      fillSeriesColor: true,
      style: {
        fontSize: "18px",
      },
    },
  };

  const optionsSPELL = {
    ...options,
    labels: [],
  };
  const optionsStakedSPELL = {
    ...options,
    labels: [],
  };
  const optionsMKR = {
    ...options,
    labels: [],
  };

  let totalSpellHolders = 0;
  let seriesSPELL = [];
  let seriesStakedSPELL = [];
  let seriesMKR = [];

  SPELLHolders.forEach((network) => {
    seriesSPELL.push(network.holders);
    totalSpellHolders += network.holders;
    optionsSPELL.labels.push(network.chainName);
  });
  sSPELLHolders.forEach((network) => {
    seriesStakedSPELL.push(network.holders);
    optionsStakedSPELL.labels.push(network.chainName);
  });
  MKRHolders.forEach((network) => {
    seriesMKR.push(network.holders);
    optionsMKR.labels.push(network.chainName);
  });

  return (
    <div className="Holders-Graph-container">
      <div className="Holders-Graph-item">
        <div className="Holders-Graph-title">{"sSPELL Holders"}</div>
        {sSPELLHolders.map((network) => (
          <NumberFormat
            key={network.chainId}
            value={network.holders}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        ))}
      </div>
      <div className="Holders-Graph-item">
        <h3 className="Holders-Graph-title">{"SPELL Holders by chain"}</h3>
        <NumberFormat
          suffix=" total"
          value={totalSpellHolders}
          displayType="text"
          thousandSeparator
          renderText={(value) => <p className="Flip-panel-label">{value}</p>}
        />
        <Chart
          options={optionsSPELL}
          series={seriesSPELL}
          type="donut"
          width={`${getGraphSizeForResponsive(width)}px`}
        />
      </div>
      <div className="Holders-Graph-item">
        <div className="Holders-Graph-title">{"MKR Holders"}</div>
        {MKRHolders.map((network) => (
          <NumberFormat
            key={network.chainId}
            value={network.holders}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        ))}
      </div>
    </div>
  );
};

HoldersGraph.propTypes = {
  SPELLHolders: PropTypes.array,
  sSPELLHolders: PropTypes.array,
  MKRHolders: PropTypes.array,
};
export default HoldersGraph;
