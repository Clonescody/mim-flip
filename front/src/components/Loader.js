import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import PropTypes from "prop-types";
import colors from "../styles/colors";

const MyLoader = ({ width = 100, height = 100 }) => (
  <Loader type="Puff" color={colors.secondary} height={height} width={width} />
);

MyLoader.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
};

export default MyLoader;
