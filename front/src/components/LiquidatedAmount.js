import "../styles/LiquidatedAmount.css";
import { useState } from "react";
import MyLoader from "./Loader";
import config from "../config/config";
import NumberFormat from "react-number-format";

const LiquidatedAmount = () => {
  const [liquidationsByChain, setLiquidationsByChain] = useState([]);
  const [totalValueLiquidated, setTotalValueLiquidated] = useState(null);
  const [address, setAddress] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetchLiquidations = async () => {
    setIsLoading(true);
    try {
      const responseLiquidations = await fetch(
        `${config.backend}/getTotalUserLiquidations`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ address }),
        }
      );
      const liquidationsJSON = await responseLiquidations.json();
      setLiquidationsByChain(liquidationsJSON.totalByChain);
      setTotalValueLiquidated(parseFloat(liquidationsJSON.totalLiquidated));
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      setError(`${e}`);
      console.log("Error staking screen : ", e);
    }
  };

  if (error !== null) {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  const handleAddressChange = (evt) => {
    if (
      evt.target.value.length === 0 ||
      (address !== "" && evt.target.value !== address)
    ) {
      setLiquidationsByChain([]);
      setTotalValueLiquidated(null);
    }
    setAddress(evt.target.value);
  };

  return (
    <div className="Liquidated-container">
      <p className="Liquidated-title">Liquidations overview</p>
      <input
        placeholder="Enter your address"
        className="Liquidated-address-input"
        type="string"
        minLength={42}
        maxLength={42}
        value={address}
        onChange={handleAddressChange}
      />
      {totalValueLiquidated === null && (
        <button
          className="Liquidated-submit-button"
          disabled={address.length < 42}
          onClick={fetchLiquidations}
        >
          {isLoading ? <MyLoader width={35} height={35} /> : "Check"}
        </button>
      )}
      {totalValueLiquidated > 0 ? (
        <div className="Liquidated-items-container">
          {liquidationsByChain.map((chain) => (
            <div className="Liquidated-chain-container" key={chain.id}>
              <div className="Liquidated-chain-name">
                <NumberFormat
                  prefix={`${chain.name} - `}
                  suffix=" MIM"
                  value={parseFloat(chain.total).toFixed(2)}
                  displayType="text"
                  thousandSeparator
                  className="Liquidated-chain-total"
                />
              </div>
              <div key={chain.id}>
                <div>
                  {chain.totalByAsset.map((asset) => (
                    <p
                      key={`${asset.asset}${chain.id}`}
                      className="Liquidated-asset-total"
                    >
                      <NumberFormat
                        suffix={` ${asset.asset} for `}
                        value={parseFloat(asset.totalCollateral).toFixed(3)}
                        displayType="text"
                        thousandSeparator
                      />
                      <NumberFormat
                        suffix=" MIM"
                        value={parseFloat(asset.total).toFixed(2)}
                        displayType="text"
                        thousandSeparator
                      />
                    </p>
                  ))}
                </div>
              </div>
            </div>
          ))}
          <p>
            <NumberFormat
              prefix="Total liquidated : "
              suffix=" MIM worth of assets"
              value={parseFloat(totalValueLiquidated).toFixed(0)}
              displayType="text"
              thousandSeparator
            />
          </p>
          <p>
            For more details visit{" "}
            <a
              className="App-link"
              href={`https://liquidated.fyi/address/${address}`}
              target="_blank"
              rel="noreferrer"
            >
              Liquidated.fyi
            </a>
          </p>
        </div>
      ) : totalValueLiquidated !== null ? (
        <p>{`You didn't get liquidated, well played !`}</p>
      ) : null}
      {totalValueLiquidated === null && (
        <p>
          Based on{" "}
          <a
            className="App-link"
            href={`https://liquidated.fyi}`}
            target="_blank"
            rel="noreferrer"
          >
            Liquidated.fyi
          </a>
        </p>
      )}
    </div>
  );
};

export default LiquidatedAmount;
