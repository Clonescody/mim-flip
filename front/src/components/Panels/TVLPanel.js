import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import Spellbook from "../../images/Spellbook.png";
import MKRLogo from "../../images/MKR-Logo.png";
import { ReactComponent as SwapLogo } from "../../images/swap.svg";

const swapSVGStyle = {
  transform: "rotate(90deg)",
};

const TVLPanel = ({ SPELLTvl, MKRTvl }) => {
  const TVLRatio = MKRTvl / SPELLTvl;
  return (
    <div className="Flip-panel-item">
      <div className="Flip-panel-text-container">
        <p className="Flip-panel-label">{"Abracadabra TVL"}</p>
        <img src={Spellbook} alt="Abracadabra Logo" width={50} height={50} />
        <NumberFormat
          prefix="$"
          value={SPELLTvl}
          displayType="text"
          thousandSeparator
          renderText={(value) => <p className="Flip-panel-label">{value}</p>}
        />
      </div>
      <div className="Flip-panel-text-container">
        <NumberFormat
          prefix="x"
          value={TVLRatio.toFixed(2)}
          displayType="text"
          thousandSeparator
          renderText={(value) => <p className="Flip-panel-label">{value}</p>}
        />
        <div style={swapSVGStyle}>
          <SwapLogo width={50} height={50} />
        </div>
      </div>
      <div className="Flip-panel-text-container">
        <p className="Flip-panel-label">{"MakerDAO TVL"}</p>
        <img src={MKRLogo} alt="MakerDAO Logo" width={50} height={50} />
        <NumberFormat
          prefix="$"
          value={MKRTvl}
          displayType="text"
          thousandSeparator
          renderText={(value) => <p className="Flip-panel-label">{value}</p>}
        />
      </div>
    </div>
  );
};

TVLPanel.propTypes = {
  SPELLTvl: PropTypes.number,
  MKRTvl: PropTypes.number,
};

export default TVLPanel;
