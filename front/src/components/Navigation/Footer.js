import "../../styles/Footer.css";
import Wizard from "../../images/Wizard.png";

const Footer = () => (
  <footer className="App-footer">
    <div>
      <img src={Wizard} alt="Wizard logo" height={70} width={70} />
    </div>
    <div className="App-links-container">
      <p>
        {"Built with magic by "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/MelenXYZ"
          rel="noreferrer"
        >
          Mélen
        </a>
        {", "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/DefiNeuro"
          rel="noreferrer"
        >
          Neuro
        </a>
        {", "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/Clonescody"
          rel="noreferrer"
        >
          Clonescody
        </a>
        {" & "}
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/0xWenMoon"
          rel="noreferrer"
        >
          Wen Moon
        </a>
      </p>
    </div>
  </footer>
);

export default Footer;
