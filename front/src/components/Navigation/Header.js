import "../../styles/Header.css";
import { Link } from "react-router-dom";

const Header = () => (
  <nav className="App-header">
    <div className="App-header-title-container">
      <h3 className="App-header-title">Abrastats</h3>
    </div>
    <div className="App-header-content-container">
      <Link className="App-header-button" to="/">
        Flippening
      </Link>
      <Link className="App-header-button" to="/fees">
        Fees
      </Link>
      <Link className="App-header-button" to="/mim">
        MIM
      </Link>
      <Link className="App-header-button" to="/staking">
        Staking
      </Link>
      <a
        href="https://zapper.fi/daos/15"
        target="_blank"
        className="App-header-button"
        rel="noreferrer"
      >
        Treasury
      </a>
      <Link className="App-header-button" to="/tools">
        Tools
      </Link>
      <Link className="App-header-button" to="/about">
        About
      </Link>
    </div>
  </nav>
);

Header.propTypes = {};

export default Header;
