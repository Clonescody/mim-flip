import "../styles/ToolsScreen.css";
import PegGraph from "../components/Graphs/PegGraph";
import LiquidatedAmount from "../components/LiquidatedAmount";

const ToolsScreen = () => {
  return (
    <div className="Tools-screen-container">
      <div className="Tools-screen-row">
        <PegGraph />
        <LiquidatedAmount />
      </div>
    </div>
  );
};

export default ToolsScreen;
