import "../styles/Treasury.css";
import { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import MyLoader from "../components/Loader";
import config from "../config/config";

const TreasuryScreen = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [treasury, setTreasury] = useState([]);
  const [error, setError] = useState(null);
  const [totalValueUsd, setTotalValueUsd] = useState(0.0);
  const [totalValueEth, setTotalValueEth] = useState(0.0);
  const [totalValueBtc, setTotalValueBtc] = useState(0.0);
  const [treasuryAddress, setTreasuryAddress] = useState("");

  const fetchTreasury = async () => {
    try {
      const responseTreasury = await fetch(`${config.backend}/getTreasury`);
      const responseTreasuryJson = await responseTreasury.json();
      setTotalValueUsd(parseInt(responseTreasuryJson.totalValueUsd));
      setTotalValueEth(responseTreasuryJson.totalValueEth);
      setTotalValueBtc(responseTreasuryJson.totalValueBtc);
      setTreasury(responseTreasuryJson.treasuryAssets);
      setTreasuryAddress(responseTreasuryJson.treasuryAddress);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError(error.toString());
      console.log("Error fetching treasury : ", error);
    }
  };

  useEffect(() => {
    fetchTreasury();
  }, []);

  if (isLoading) {
    return (
      <div className="Treasury-container">
        <MyLoader />
      </div>
    );
  }

  if (error !== null) {
    return (
      <div className="Treasury-container">
        <p>{error}</p>
      </div>
    );
  }

  const veCrv = treasury.find((asset) => asset.type === "ve-crv");

  return (
    <div className="Treasury-container">
      <p className="Treasury-Header-title">Abracadabra treasury</p>
      <div className="Treasury-Header-container">
        <div className="Treasury-Header-box">
          <p className="Treasury-Header-box-title">Treasury value</p>
          <NumberFormat
            suffix=" $"
            value={totalValueUsd}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Treasury-Box-text">{value}</p>}
          />
          <NumberFormat
            suffix=" ETH"
            value={totalValueEth}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Treasury-Box-text">{value}</p>}
          />
          <NumberFormat
            suffix=" BTC"
            value={totalValueBtc}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Treasury-Box-text">{value}</p>}
          />
        </div>
        <div className="Treasury-Header-box">
          <p className="Treasury-Header-box-title">Curve veCRV</p>
          <NumberFormat
            suffix=" $ locked"
            value={veCrv.totalValue}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Treasury-Box-text">{value}</p>}
          />
          <NumberFormat
            suffix=" veCRV voting power"
            value={veCrv.votingPowerVeCRV}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Treasury-Box-text">{value}</p>}
          />
          <NumberFormat
            prefix="CRV locked as mCRV : "
            value={veCrv.votingPowerVeCRV}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Treasury-Box-text">{value}</p>}
          />
        </div>
      </div>
      <div className="Treasury-Table-container">
        {/* <div className="Treasury-Table-header">
          <button>All</button>
          <button>Curve</button>
          <button>Yearn</button>
          <button>Stablecoins</button>
          <button>LPs</button>
        </div> */}
        <div className="Treasury-Table">
          <div className="Treasury-Table-titles">
            <p className="Treasury-Table-column">Asset</p>
            <p className="Treasury-Table-column">Amount</p>
            <p className="Treasury-Table-column">Underlying</p>
            <p className="Treasury-Table-column">Value</p>
          </div>
          {treasury
            .filter((asset) => asset.type !== "ve-crv")
            .filter((asset) => parseFloat(asset.amount) > 0)
            .map((asset) => (
              <div key={asset.name} className="Treasury-Table-row">
                <p className="Treasury-Table-column">{asset.name}</p>
                <NumberFormat
                  value={asset.amount}
                  displayType="text"
                  thousandSeparator
                  renderText={(value) => (
                    <p className="Treasury-Table-column">{value}</p>
                  )}
                />
                <p className="Treasury-Table-column">
                  {asset.underlying ? asset.underlying.name : "/"}
                </p>
                <NumberFormat
                  suffix=" $"
                  value={asset.totalValue}
                  displayType="text"
                  thousandSeparator
                  renderText={(value) => (
                    <p className="Treasury-Table-column">{value}</p>
                  )}
                />
              </div>
            ))}
        </div>
      </div>
      <p>
        Treasury address :{" "}
        <a
          className="App-link"
          href={`https://etherscan.com/address/${treasuryAddress}`}
          target="_blank"
          rel="noreferrer"
        >
          {treasuryAddress}
        </a>
      </p>
    </div>
  );
};

export default TreasuryScreen;
