import "@coreui/coreui/dist/css/coreui.min.css";
import "../styles/MIMsPanel.css";
import { useEffect, useState } from "react";
import { CFormCheck } from "@coreui/react";
import MyLoader from "../components/Loader";
import config from "../config/config";
import NumberFormat from "react-number-format";

const MIMsScreen = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [borrowables, setBorrowables] = useState([]);
  const [filteredBorrowables, setFilteredBorrowables] = useState([]);
  const [chainFilters, setChainFilters] = useState([]);
  const [showDeprecated, setShowDeprecated] = useState(false);

  const fetchBorrowableMims = async () => {
    const responseBorrowableMims = await fetch(
      `${config.backend}/getBorrowableMims`
    );
    const borrowablesMimsJSON = await responseBorrowableMims.json();
    setBorrowables(borrowablesMimsJSON.cauldrons);
    setFilteredBorrowables(
      borrowablesMimsJSON.cauldrons.filter(
        (cauldron) => cauldron.deprecated === false
      )
    );
    setIsLoading(false);
  };

  useEffect(() => {
    fetchBorrowableMims();
  }, []);

  useEffect(() => {
    filterCauldrons();
  }, [showDeprecated]);

  useEffect(() => {
    filterCauldrons();
  }, [chainFilters]);

  const filterCauldrons = () => {
    if (!showDeprecated) {
      if (chainFilters.length > 0) {
        setFilteredBorrowables(
          borrowables.filter(
            (cauldron) =>
              cauldron.deprecated === false &&
              chainFilters.includes(cauldron.chain)
          )
        );
      } else {
        setFilteredBorrowables(
          borrowables.filter((cauldron) => cauldron.deprecated === false)
        );
      }
    } else {
      if (chainFilters.length > 0) {
        setFilteredBorrowables(
          borrowables.filter((cauldron) =>
            chainFilters.includes(cauldron.chain)
          )
        );
      } else {
        setFilteredBorrowables(borrowables);
      }
    }
  };

  const toggleDeprecatedFilter = () => {
    setShowDeprecated(!showDeprecated);
  };

  const filterWithChain = (chain) => {
    if (chainFilters.includes(chain)) {
      setChainFilters(chainFilters.filter((item) => item !== chain));
    } else {
      setChainFilters([...chainFilters, chain]);
    }
  };

  if (isLoading) {
    return (
      <div className="MIM-Loading-container">
        <MyLoader />
      </div>
    );
  }

  let total = {
    borrowed: 0,
    borrowable: 0,
  };
  borrowables.map((cauldron) => {
    total.borrowed += parseInt(cauldron.borrowedMIMs);
    if (!cauldron.deprecated) {
      total.borrowable += parseInt(cauldron.borrowableMIMs);
    }
  });

  return (
    <div className="MIM-container">
      <p className="MIM-container-title">MIM supply breakdown</p>
      <div className="MIM-Header-container">
        <div className="MIM-Header-box">
          <p>Total MIM borrowed</p>
          <NumberFormat
            suffix=" MIM"
            value={total.borrowed}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="MIM-Box-text">{value}</p>}
          />
        </div>
        <div className="MIM-Header-box">
          <p>Total MIM available</p>
          <NumberFormat
            suffix=" MIM"
            value={total.borrowable}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="MIM-Box-text">{value}</p>}
          />
        </div>
      </div>
      <div>
        <CFormCheck
          inline
          id="inlineCheckbox1"
          label="Show deprecated"
          onClick={() => toggleDeprecatedFilter()}
        />
        <CFormCheck
          inline
          id="inlineCheckbox2"
          label="Ethereum"
          onClick={() => filterWithChain("Ethereum")}
        />
        <CFormCheck
          inline
          id="inlineCheckbox3"
          label="Arbitrum"
          onClick={() => filterWithChain("Arbitrum")}
        />
        <CFormCheck
          inline
          id="inlineCheckbox4"
          label="Avalanche"
          onClick={() => filterWithChain("Avalanche")}
        />
        <CFormCheck
          inline
          id="inlineCheckbox5"
          label="Fantom"
          onClick={() => filterWithChain("Fantom")}
        />
        <CFormCheck
          inline
          id="inlineCheckbox6"
          label="Binance"
          onClick={() => filterWithChain("Binance")}
        />
      </div>
      <div className="MIM-Table-container">
        <div className="MIM-Table">
          <div className="MIM-Table-titles">
            <p className="MIM-Table-column">Asset</p>
            <p className="MIM-Table-column">Chain</p>
            <p className="MIM-Table-column">Borrowed</p>
            <p className="MIM-Table-column">Available</p>
          </div>
          {filteredBorrowables.map((asset) => (
            <div key={asset.name} className="MIM-Table-row">
              <p className="MIM-Table-column">{asset.name}</p>
              <p className="MIM-Table-column">{asset.chain}</p>
              <NumberFormat
                value={asset.borrowedMIMs}
                displayType="text"
                thousandSeparator
                renderText={(value) => (
                  <p className="MIM-Table-column">{value}</p>
                )}
              />
              <NumberFormat
                value={asset.deprecated ? 0 : asset.borrowableMIMs}
                displayType="text"
                thousandSeparator
                renderText={(value) => (
                  <p className="MIM-Table-column">{value}</p>
                )}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default MIMsScreen;
