import "../styles/StakingScreen.css";
import { useEffect, useState } from "react";
import config from "../config/config";
import { getHistoricalRatioMock } from "../utils/getHistoricalRatioMock";
import MyLoader from "../components/Loader";
import StakedRatioGraph from "../components/Graphs/StakedRatioGraph";
import StakedRatioCalculator from "../components/StakedRatioCalculator";

const historicalRatioModel = getHistoricalRatioMock();

const StakingScreen = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [historicalRatio, setHistoricalRatio] = useState(historicalRatioModel);
  const [error, setError] = useState(null);

  const fetchInfos = async () => {
    setIsLoading(true);
    try {
      const responseHistoricalRatio = await fetch(
        `${config.backend}/getHistoricalStakedSpellRatio`
      );
      const historicalRatioJSON = await responseHistoricalRatio.json();
      if (historicalRatioJSON.stakedRatioHistory !== null) {
        setHistoricalRatio(historicalRatioJSON.stakedRatioHistory);
      }
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      setError(`${e}`);
      console.log("Error staking screen : ", e);
    }
  };

  useEffect(() => {
    fetchInfos();
  }, []);

  if (error !== null) {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  if (isLoading || historicalRatio.length === 0) {
    return (
      <div className="App-flip-panel">
        <MyLoader />
      </div>
    );
  }

  return (
    <div className="Staking-screen-container">
      <div>
        <p>
          For details on staking distribution, visit{" "}
          <a
            className="App-link"
            href="https://wenmerl.in/"
            target="_blank"
            rel="noreferrer"
          >
            Wenmerl.in
          </a>
        </p>
      </div>
      <div className="Staking-screen-row">
        <StakedRatioGraph historicalRatio={historicalRatio} />
        <StakedRatioCalculator historicalRatio={historicalRatio} />
      </div>
    </div>
  );
};

export default StakingScreen;
