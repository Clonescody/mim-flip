import "../styles/FeesScreen.css";
import { useEffect, useState } from "react";
import config from "../config/config";
import MyLoader from "../components/Loader";
import CauldronsGraph from "../components/Graphs/CauldronsGraph";
import DailyFeesGraph from "../components/Graphs/DailyFeesGraph";

const FeesScreen = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [dailyFees, setDailyFees] = useState([]);
  const [error, setError] = useState(null);

  const fetchInfos = async () => {
    setIsLoading(true);
    try {
      const responseDailyFees = await fetch(`${config.backend}/getDailyFees`);
      const dailyFeesJSON = await responseDailyFees.json();
      if (dailyFeesJSON.dailyFees !== null) {
        setDailyFees(dailyFeesJSON.dailyFees);
      }

      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      setError(`${e}`);
      console.log("Error fees panel : ", e);
    }
  };

  useEffect(() => {
    fetchInfos();
  }, []);

  if (error !== null) {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  if (isLoading || dailyFees.length === 0) {
    return (
      <div className="App-flip-panel">
        <MyLoader />
      </div>
    );
  }

  return (
    <div className="Fees-screen-container">
      <div className="Fees-screen-row">
        <DailyFeesGraph dailyFees={dailyFees} />
      </div>
      <div className="Fees-screen-row">
        <CauldronsGraph type="all" />
        <CauldronsGraph type="ethereum" />
        <CauldronsGraph type="avalanche" />
      </div>
      <div className="Fees-screen-row">
        <CauldronsGraph type="fantom" />
        <CauldronsGraph type="arbitrum" />
        <CauldronsGraph type="binance" />
      </div>
    </div>
  );
};

export default FeesScreen;
