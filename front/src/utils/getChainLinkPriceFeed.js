import moment from "moment";

export const getChainLinkPriceFeed = async (
  token,
  dayInterval,
  timeInterval
) => {
  try {
    return fetch(
      `https://market.link/v1/metrics/api/v1/query_range?query=avg(feeds_latest_answer%7Bfeed_address%3D~%22(%3Fi)${
        token.feedAddress
      }%22%2C%20network_id%3D~%221%7C%22%7D)%20by%20(feed_address)%20%2F%20100000000&start=${moment
        .tz("America/Costa_Rica")
        .subtract(dayInterval, "d")
        .unix()}&end=${moment
        .tz("America/Costa_Rica")
        .unix()}&step=${timeInterval}&days=${dayInterval}`
    )
      .then((response) => response.json())
      .then((data) => {
        return data.data.result[0].values;
      });
  } catch (error) {
    return error;
  }
};
