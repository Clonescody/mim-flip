import "./styles/App.css";
import { Route, Routes } from "react-router";
import Header from "./components/Navigation/Header";
import Footer from "./components/Navigation/Footer";
import FlipScreen from "./screens/FlipScreen";
import FeesScreen from "./screens/FeesScreen";
import MIMsScreen from "./screens/MIMsScreen";
import AboutScreen from "./screens/AboutScreen";
// import TreasuryScreen from "./screens/TreasuryScreen";
import StakingScreen from "./screens/StakingScreen";
import ToolsScreen from "./screens/ToolsScreen";

const App = () => {
  return (
    <div className="App">
      <Header />
      <div className="App-container">
        <Routes>
          <Route path="/" element={<FlipScreen />} />
          <Route path="fees" element={<FeesScreen />} />
          <Route path="mim" element={<MIMsScreen />} />
          <Route path="staking" element={<StakingScreen />} />
          {/* <Route path="treasury" element={<TreasuryScreen />} /> */}
          <Route path="tools" element={<ToolsScreen />} />
          <Route path="about" element={<AboutScreen />} />
        </Routes>
      </div>
      <Footer />
    </div>
  );
};

export default App;
